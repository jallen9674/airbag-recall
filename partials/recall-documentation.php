
    <!--  //Begin Demo Notifications & Info. -->
    <div class="sample-recall-messages">

        <h2 style="margin-top: 0;">Misc. Widget Information for Reference</h2>

        <h3>Demo VIN Numbers</h3>
        <ul>
            <li>Example VIN (No Recalls): 1FD7W3BTXFEC24406</li>
            <li>Example VIN (With Recalls): WAUNF78P36A032617</li>
            <li>Example VIN (With Recalls): 1C3LN59L47X070986</li>

        </ul>

        <h3>Sample Messages for Recall Status</h3>
        <div class="standard-alert alert-green">
            <div class="alert-heading">
                Good News
            </div>
            <div class="alert-content">
                No air bag recalls are associated with the VIN number entered.
            </div>
        </div>
        <div class="standard-alert alert-yellow">
            <div class="alert-heading">
                Caution
            </div>
            <div class="alert-content">
                <p>Air bag recalls were found for your vehicle. It is important to contact your local delarship and get your vehicle inspected and fixed. The repairs should be free of charge, if you are billed by a dealer for recall repairs, contact us today.</p>
                <p>To view information regarding the open recalls for your vehicle, please expand the boxes below.</p>
            </div>
        </div>

    </div>
    <!--  //End Demo Notifications & Info. -->
