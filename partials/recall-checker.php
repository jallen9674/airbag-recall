    <div class="vin-lookup-wrapper">
        <div class="lookup-outer">
            <div class="lookup-inner">

                <div class="lookup-title">
                    Airbag Recall Lookup
                </div>


                <!--  //Begin Input Form -->

                <div class="lookup-form">
                    <h2>Enter Your Vehicle's VIN Number Here</h2>
                    <form class="vin-entry-form" id="vin-entry-form" data-parsley-validate>
                        <div class="vin-number-input-wrapper">
                            <input type="text" name="vin-number" class="vin-number-input" id="vin-number-input" placeholder="Vehicle VIN Number (Required)" required
                            aria-label="Enter VIN Number Input Field"
                            data-parsley-pattern="/^[A-Z0-9^IOQioq_]{11}(\d{6}|[a-zA-z0-9]{6})$/i"
                            data-parsley-error-message="Please Enter a Valid VIN Number">
                        </div>

                        <input type="submit" value="Check for Airbag Recalls" class="vin-submit">


                    </form>
                </div>
                <!--  //End Input Form -->

                <!--  //Begin Returned Data From API -->

                <div class="lookup-results">
                    <!-- <h2>Airbag Recall Status</h2> -->

                    <div class="car-information">

                    </div>
                    <div class="car-recalls">
                        <!-- Enter a VIN number into the form above to see if your vehicle has open airbag recalls. -->
                    </div>

                </div>
                <!--  //End Returned Data From API -->

            </div>

        </div>
    </div>
