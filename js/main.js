//Example VIN: 1FD7W3BTXFEC24406
//Example VIN w/ Airbag Recall: WAUNF78P36A032617

jQuery(document).ready(function ($) {

    //Prevent Default Submit Functionality
    $(".vin-entry-form").submit(function (e) {
        e.preventDefault();
    });


    //Start Things Up
    var vinNumber,
        carData,
        carMake,
        carModel,
        carModelYear,
        carRecallData,
        carRecallCount,
        carData,
        carRecallHTML;

    //Recall Messages

    var recallStatusSuccess = '<div class="standard-alert alert-green"><div class="alert-heading">Good News!</div><div class="alert-content">No air bag recalls are associated with the VIN number entered.</div></div>';

    var recallStatusCaution = '<div class="standard-alert alert-yellow"><div class="alert-heading">Caution</div><div class="alert-content"><p>Air bag recalls were found for your vehicle. It is important to contact your local dealership and get your vehicle inspected and fixed. The repairs should be free of charge; if you are billed by a dealer for recall repairs, contact us today.</p><p>To view information regarding the open recalls for your vehicle, please expand the boxes below.</p></div></div>';

    //Get VIN For API Lookup
    $('.vin-submit').on('click', function(){

        $('.vin-entry-form').parsley().validate();

        //Check if Is Validated By Parsley

        if ( $('.vin-number-input').hasClass('parsley-success') ) {

            vinNumber = $('.vin-number-input').val();
            //console.log('VIN Entered: ' + vinNumber);
            //console.log('URL: ' + 'https://vpic.nhtsa.dot.gov/api/vehicles/decodevin/' + vinNumber + '?format=json');

            //Display Loading Icon & Hide Any Stale Car Information
            $('.car-information').html(
                ''
            );
            $('.car-recalls').html(
                '<div class="car-recall-loading-spinner"><i class="fas fa-cog fa-spin"></i></div>'
            );

            //Lookup Make, Year, Model Based on VIN
            jQuery.ajax({
                url: "https://vpic.nhtsa.dot.gov/api/vehicles/decodevin/" + vinNumber + "?format=json",
                type: "GET",
                async: false,
                dataType: 'json',

                success: function (resultData) {
                    //console.log('VIN Lookup Success: ' + resultData['Message']);

                    carData = resultData['Results'];

                    //Get Make, Model, and Model Year from Data
                    for (var key in carData) {

                        if (carData[key]['Variable'] == "Make" ) {
                            carMake = carData[key]['Value'];
                            //console.log('Make Found: ' + carMake);
                        }

                        if (carData[key]['Variable'] == "Model") {
                            carModel = carData[key]['Value'];
                            //console.log('Model Found: ' + carModel);
                        }

                        if (carData[key]['Variable'] == "Model Year") {
                            carModelYear = carData[key]['Value'];
                            //console.log('Model Year Found: ' + carModelYear);
                        }

                    }

                    //Update DOM
                    if ((carMake !== null) && (carModel !== null) && (carModelYear !== null)) {


                        //Lookup Recalls Based on Make, Model, and Model Year
                        jQuery.ajax({
                            url: "https://one.nhtsa.gov/webapi/api/Recalls/vehicle/modelyear/" + carModelYear + "/make/" + carMake + "/model/" + carModel + "?format=json",

                            //Test Data (No Air Bag Recalls)
                            //url: "https://one.nhtsa.gov/webapi/api/Recalls/vehicle/modelyear/2000/make/saturn/model/LS?format=jsonp",

                            type: "GET",
                            async: false,
                            dataType: 'jsonp',
                            xhrFields: {
                                withCredentials: false
                            },

                            success: function (resultData) {
                                //console.log('Recall URL: ' + 'https://one.nhtsa.gov/webapi/api/Recalls/vehicle/modelyear/' + carModelYear + '/make/' + carMake + '/model/' + carModel + '?format=json');
                                //console.log('Recall Information Found: ' + resultData['Message']);

                                carRecallData = resultData['Results'];
                                carRecallCount = 0;
                                carRecallHTML = '';

                                //Get Make, Model, and Model Year from Data
                                for (var key in carRecallData) {
                                    //console.log(carRecallData[key]['Component']);

                                    //Check if Recall Notice Contains "Air Bag"
                                    if ( carRecallData[key]['Component'].match(/air bag/i) ) {
                                        carRecallCount++;
                                        carRecallHTML += '<div class="car-recall-result"><div class="car-recall-title"><strong>' + carRecallData[key]['Component'] + '</strong><i class="far fa-plus-square car-recall-accordion-toggle"></i></div><div class="car-recall-content">' + carRecallData[key]['Summary'] + '<br><br>' + carRecallData[key]['Conequence'] + '<br><br>' + carRecallData[key]['Remedy'] + '</div></div>';
                                    }

                                }

                                //Update DOM

                                if ( carRecallCount == 0  ) {
                                    $('.car-information').html(
                                        'Car Information: ' + carModelYear + ' ' + carMake + ' ' + carModel
                                    );
                                    //No Recalls Found
                                    $('.car-recalls').html(
                                        recallStatusSuccess
                                    );
                                } else {
                                    $('.car-information').html(
                                        'Car Information: ' + carModelYear + ' ' + carMake + ' ' + carModel
                                    );
                                    $('.car-recalls').html(
                                        recallStatusCaution +
                                        '<div class="car-recall-list-title">Open Air Bag Recalls</div><div class="car-recall-list">' + carRecallHTML + '</div>'
                                    );

                                    addAccordionClick();
                                }

                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                //console.log('Recall Information Lookup Error');

                                //Update DOM
                                $('.car-information').html(
                                        'Car Information: ' + carModelYear + ' ' + carMake + ' ' + carModel
                                );
                                $('.car-recalls').html(
                                    'There was an error looking up recall information. Please try again later.'
                                );
                            },

                            timeout: 120000,
                        }); //End Recall Lookup Request


                    } else {
                        //VIN Lookup Failed
                        $('.car-information').html(
                            ''
                        );
                        $('.car-recalls').html(
                            'No vehicle information found for VIN number.<br>Please double check the VIN number entered and try again.'
                        );
                    }


                },

                error: function (jqXHR, textStatus, errorThrown) {
                    //console.log('VIN Lookup Error');

                    //Update DOM
                    $('.car-information').html(
                        'VIN Lookup Error'
                    );
                    $('.car-recalls').html(
                        'Recall Information Lookup Error: Bad VIN'
                    );

                },

                timeout: 120000,
            }); //End VIN Lookup Request

        } //End Parsley If
        else {
            //Reset HTML
            $('.car-information').html(
                ''
            );
            $('.car-recalls').html(
                'Please enter a valid VIN number.'
            );
        }


    }); //End On Click

    //Displayed Results Accordion Functionality
    function addAccordionClick(){

        $('.car-recall-title').on('click', function () {

            //Update Font Awesome Button
            $(this).find($('.far')).toggleClass('fa-plus-square').toggleClass('fa-minus-square').parent().siblings('.car-recall-content').toggleClass('visible');

            //Hide/Show Slide Animation
            if ( $(this).siblings('.car-recall-content').hasClass('visible') ) {
                $(this).siblings('.car-recall-content').slideDown(250);
            } else {
                $(this).siblings('.car-recall-content').slideUp(250);
            }

        });


    }

    addAccordionClick();

    //Update Input Field on Validation

    $(".vin-number-input-wrapper input").bind('cssClassChanged', function () {
        if ($(this).hasClass('parsley-success')) {
            $(this).parent().addClass('parsley-success');
            $(this).parent().removeClass('parsley-error');
        }
        if ($(this).hasClass('parsley-error')) {
            $(this).parent().addClass('parsley-error');
            $(this).parent().removeClass('parsley-success');
        }

    });


});




/*--------------------------------------------
Do Something on Class Change
---------------------------------------------*/
(function () {
    // Your base, I'm in it!
    var originalAddClassMethod = jQuery.fn.addClass;

    jQuery.fn.addClass = function () {
        // Execute the original method.
        var result = originalAddClassMethod.apply(this, arguments);

        // trigger a custom event
        jQuery(this).trigger('cssClassChanged');

        // return the original result
        return result;
    }
})();
