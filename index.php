<html>
    <head>
        <meta charset="UTF-8">
        <title>VIN Checker Demo | John Allen</title>
        <meta name="description" content="Airbag Recall Lookup Checker | John Allen">
        <meta type="robots" content="noindex,nofollow" />
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" type="text/css" href="./css/styles.css">
        <script src="./js/vendor/modernizr-3.5.0.min.js" async defer></script>

    </head>
    <body>

       <div class="view-code">
            <a href="https://bitbucket.org/jallen9674/airbag-recall/src/master/" target="_blank"><img alt="View Source" src="./images/bitbucket-icon.svg"> View Source on Bitbucket</a>
        </div>
        <div class="portfolio-return">
            <a href="https://jsallen.com/portfolio/">&laquo; Return to Portfolio</a>
        </div>

        <div class="vin-demo">
            <?php require("./partials/recall-checker.php"); ?>
        
        </div>

        <div class="vin-doc">
            <?php require("./partials/recall-documentation.php"); ?>
        </div>

        <div class="view-code">
            <a href="https://bitbucket.org/jallen9674/airbag-recall/src/master/" target="_blank"><img alt="View Source" src="./images/bitbucket-icon.svg"> View Source on Bitbucket</a>
        </div>

        <div class="portfolio-return">
            <a href="https://jsallen.com/portfolio/">&laquo; Return to Portfolio</a>
        </div>


        <?php //JS ?>
        <script src="./js/vendor/jquery-3.2.1.min.js"></script>
        <script src="./js/vendor/parsey-2.8.0.js"></script>
        <script src="./js/main.js"></script>

    </body>
</html>